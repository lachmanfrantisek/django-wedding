import markdown
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Article(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("Link-name"))
    title = models.CharField(max_length=200, verbose_name=_("Title"))
    text = models.TextField(verbose_name=_("Markdown-formatted text content"))

    def __str__(self):
        return self.title

    def get_html_list(self):
        md_list = self.text.split('\r\n~~~\r\n')
        return [markdown.markdown(md) for md in md_list]
