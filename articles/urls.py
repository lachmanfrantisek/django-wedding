from django.conf.urls import url

from articles import views

app_name = 'articles'

urlpatterns = [
    url(r'^$', views.ArticleIndexView.as_view(),
        name='list'),
    url(r'^id/(?P<pk>[0-9]+)/$', views.ArticleUpdate.as_view(),
        name='article-update'),
    url(r'^id/(?P<pk>[0-9]+)/delete/$', views.ArticleDelete.as_view(),
        name='article-delete'),
    url(r'^add/$', views.ArticleCreate.as_view(), name='create'),
    url(r'^', views.articles, name='articles'),
]
