import re

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import UpdateView, CreateView, DeleteView

from articles.models import Article


def articles(request):
    article_match = re.match(r'/articles/([^/?]*)/?', request.path)
    if article_match:
        article = Article.objects.filter(name=article_match.group(1))
        if len(article) == 1:
            return render(request, 'articles/article.html',
                          context={
                              'article': article[0],
                              'title': article[0].title,
                          })
    return render(request, 'articles/no_article.html', {})


class ArticleUpdate(UpdateView):
    model = Article
    fields = ['name', 'title', 'text']

    def get_success_url(self):
        return "/articles/{}/".format(self.get_object().name)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["title"] = "Upravit článek: "
        return ctx


class ArticleCreate(CreateView):
    model = Article
    fields = ['name', 'title', 'text']
    success_url = reverse_lazy('articles:list')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["title"] = "Vytvořit článek"
        return ctx


class ArticleDelete(DeleteView):
    model = Article
    success_url = reverse_lazy('articles:list')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["title"] = "Smazat článek: "
        return ctx

class ArticleIndexView(generic.ListView):
    model = Article
    ordering = '-title'
