from django.contrib import admin
from .models import Gift, Preamble

admin.site.register(Gift)
admin.site.register(Preamble)
