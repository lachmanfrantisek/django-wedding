from django.conf.urls import url
from . import views

app_name = 'gifts'

urlpatterns = [
    url(r'^$', views.GiftIndexView.as_view(),
        name='list'),

    url(r'^preamble/add/$', views.PreambleCreate.as_view(),
        name='preamble-create'),
    url(r'^preamble/(?P<pk>[0-9]+)/$', views.PreambleUpdate.as_view(),
        name='preamble-update'),
    url(r'^preamble/(?P<pk>[0-9]+)/delete/$', views.PreambleDelete.as_view(),
        name='preamble-delete'),

    url(r'^add/$', views.GiftCreate.as_view(),
        name='create'),
    url(r'^(?P<pk>[0-9]+)/$', views.GiftUpdate.as_view(),
        name='update'),
    url(r'^(?P<pk>[0-9]+)/delete/$', views.GiftDelete.as_view(),
        name='delete'),
    url(r'^[0-9]+/reserve/$', views.GiftReserveView.as_view(),
        name='reserve'),
]
