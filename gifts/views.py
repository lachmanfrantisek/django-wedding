import re

from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from django.template.loader import get_template
from django.views.generic import CreateView, UpdateView, DeleteView, TemplateView, ListView
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from .models import Gift, Preamble

class GiftIndexView(ListView):
    model = Gift
    context_object_name = 'all_gifts'
    ordering = 'title'
    message = None

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['message'] = self.message
        ctx['title'] = "(Milo)dary"
        ctx['preambles'] = Preamble.objects.order_by('id')
        return ctx

    def get_queryset(self):
        set = super().get_queryset()
        return set




class GiftCreate(CreateView):
    model = Gift
    fields = ['title', 'description', 'capacity', 'image']

    def get_success_url(self):
        return reverse_lazy('gifts:list') + "#content"


class GiftUpdate(UpdateView):
    model = Gift
    fields = ['title', 'description', 'capacity', 'reserved', 'image']

    def get_success_url(self):
        return reverse_lazy('gifts:list') + "#content"


class GiftDelete(DeleteView):
    model = Gift

    def get_success_url(self):
        return reverse_lazy('gifts:list') + "#content"


class GiftReserveView(TemplateView):
    def get(self, request, *args, **kwargs):
        gift_match = re.match(r'/gift/([0-9]+)/reserve/?', request.path)
        if gift_match:
            try:
                gift = Gift.objects.get(pk=gift_match.group(1))
            except:
                giw = GiftIndexView()
                giw.request = request
                giw.message = _("Gift not found")
                return giw.get(request)
        return render(request, 'gifts/gift_confirm.html', {'confirm_title': 'Potvrdit rezervaci daru',
                                                    'confirm_text': "Zarezervovat",
                                                    "object": gift})

    def post(self, request):
        message = "Dar se nepodařilo zarezervovat!"
        try:
            gift_match = re.match(r'/gift/([0-9]+)/reserve/?', request.path)
            if gift_match:
                gift = Gift.objects.get(pk=gift_match.group(1))

                if gift.free_count == "infinite" or gift.free_count > 0:
                    gift.reserved += 1
                    gift.save()
                    message = _("Gift reserved!")

                    for user in User.objects.all():
                        if user.groups.filter(name='notification').exists():

                            plaintext = get_template('gifts/email.txt')
                            html = get_template('gifts/email.html')
                            d = {'gift': gift}
                            text_content = plaintext.render(d)
                            html_content = html.render(d)

                            msg = EmailMultiAlternatives(subject=_("Gifts"),
                                                         from_email="svatba@frantisekadominika.tk",
                                                         reply_to=["lachmanfrantisek@gmail.com"],
                                                         to=[user.email],
                                                         body=text_content)

                            msg.attach_alternative(html_content, "text/html")
                            msg.send(fail_silently=True)

                else:
                    message = _("Gift cannot be reserved!")
        except Exception as ex:
            print(ex)

        giw = GiftIndexView()
        giw.request = request
        giw.message = message
        return giw.get(request)

class PreambleCreate(CreateView):
    model = Preamble
    fields = ['text']

    def get_success_url(self):
        return reverse_lazy('gifts:list') + "#content"


class PreambleUpdate(UpdateView):
    model = Preamble
    fields = ['id', 'text']

    def get_success_url(self):
        return reverse_lazy('gifts:list') + "#content"


class PreambleDelete(DeleteView):
    model = Preamble

    def get_success_url(self):
        return reverse_lazy('gifts:list') + "#content"
