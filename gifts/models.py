import markdown

from django.db import models
from django.utils.translation import ugettext_lazy as _

class Gift(models.Model):
    title = models.CharField(max_length=200, verbose_name=_("Title"))
    description = models.TextField(verbose_name=_("Description"))
    capacity = models.IntegerField(default=1, verbose_name=_("Capacity"))
    reserved = models.IntegerField(default=0)
    image = models.FileField()

    def __str__(self):
        return self.title

    def get_html_description(self):
        return markdown.markdown(self.description)

    @property
    def free_count(self):
        if self.capacity == -1:
            return "infinite"
        return self.capacity - self.reserved

    @property
    def enabled(self):
        if self.capacity == -1:
            return True
        return self.capacity > self.reserved


class Preamble(models.Model):
    text = models.TextField(verbose_name=_("Text"))

    def __str__(self):
        return self.text[:10]

    def get_html_list(self):
        md_list = self.text.split('\r\n~~~\r\n')
        return [markdown.markdown(md) for md in md_list]

    def get_html_first(self):
        md_list = self.text.split('\r\n~~~\r\n')
        return markdown.markdown(md_list[0])

    def get_html_tail(self):
        md_list = self.text.split('\r\n~~~\r\n')
        return [markdown.markdown(md) for md in md_list[1:]]
