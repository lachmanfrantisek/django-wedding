from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
from django.utils.translation import ugettext_lazy as _

def upload_media(request):

    try:
        if request.method == 'POST' and request.FILES['myfile']:
            myfile = request.FILES['myfile']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            return render(request, 'mediafiles/upload.html', {
                'uploaded_file_url': uploaded_file_url
            })
        return render(request, 'mediafiles/upload.html')
    except:
        return render(request, 'mediafiles/upload.html',
                {"error" : _("No file uploaded.")})
