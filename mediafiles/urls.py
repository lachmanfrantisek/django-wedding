from django.conf.urls import url
from . import views

app_name = 'mediafiles'

urlpatterns = [
    url(r'^upload/$', views.upload_media,
        name='upload-media'),
]
