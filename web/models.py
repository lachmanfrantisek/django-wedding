import markdown
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class News(models.Model):
    title = models.CharField(max_length=200, verbose_name=_("Title"))
    text = models.TextField(verbose_name=_("Text"))
    date = models.DateTimeField(default=timezone.now, verbose_name=_("Date"))
    send_date = models.DateTimeField(default=None, null=True, blank=True,
                                     verbose_name=_("Send date"))

    def get_html_text(self):
        return markdown.markdown(self.text)

    def __str__(self):
        return self.title


class Question(models.Model):
    author = models.CharField(max_length=100, verbose_name=_("Author"))
    title = models.CharField(max_length=200, verbose_name=_("Title"))
    text = models.TextField(verbose_name=_("Text"))
    date = models.DateTimeField(default=timezone.now, verbose_name=_("Date"))
    answer = models.ForeignKey('Answer',
                               blank=True,
                               null=True,
                               on_delete=models.SET_NULL,
                               verbose_name=_("Answer"))

    def __str__(self):
        return "{} ({}) {}...".format(self.title, self.author, self.text[:30])


class Answer(models.Model):
    author = models.ForeignKey('auth.User', verbose_name=_("Author"), on_delete=models.SET_NULL,
                               null=True)
    text = models.TextField(verbose_name=_("Text"))
    date = models.DateTimeField(default=timezone.now, verbose_name=_("Date"))

    def __str__(self):
        return "{}... ({})".format(self.text[:30], self.author)


class Registration(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("Name/names"))
    people_count = models.IntegerField(default=1, verbose_name=_("Number of people"))
    go_to_lunch = models.BooleanField(default=False, verbose_name=_(
        "I am invited to the lunch and I am going to come."))
    go_to_party = models.BooleanField(default=False, verbose_name=_(
        "I am invited to the party and I am going to come."))
    mail = models.EmailField(verbose_name=_("Email adress, that I am used to read"))
    date = models.DateTimeField(default=timezone.now, verbose_name=_("Date"))

    def __str__(self):
        return "{} ({})".format(self.name, self.people_count)


class AnnounceMail(models.Model):
    subject = models.CharField(max_length=100, verbose_name=_("Subject"))
    go_to_lunch = models.BooleanField(default=False, verbose_name=_(
        "Send only to people going to a lunch."))
    go_to_party = models.BooleanField(default=False, verbose_name=_(
        "Send only to people going to a party."))
    text = models.TextField(verbose_name=_("Text"))
    date = models.DateTimeField(default=timezone.now, verbose_name=_("Date"))

    def __str__(self):
        return "{} ({})".format(self.subject, self.date)
