from django.conf.urls import url

from . import views

app_name = 'web'

urlpatterns = [
    url(r'^$', views.main_page, name='index'),
    url(r'^questions/answer/$', views.question_answer, name='question_answer'),

    # News
    url(r'^news/add/$', views.NewsCreate.as_view(),
        name='news-create'),
    url(r'^news/$', views.NewsIndexView.as_view(),
        name='news-list'),
    url(r'^news/(?P<pk>[0-9]+)/$', views.NewsUpdate.as_view(),
        name='news-update'),
    url(r'^news/(?P<pk>[0-9]+)/delete/$', views.NewsDelete.as_view(),
        name='news-delete'),
    url(r'^news/feed/$', views.NewsFeed(),
        name='news-feed'),

    # Questions
    url(r'^question/add/$', views.QuestionCreate.as_view(),
        name='question-create'),
    url(r'^question/$', views.QuestionIndexView.as_view(),
        name='question-list'),
    url(r'^question/(?P<pk>[0-9]+)/$', views.QuestionUpdate.as_view(),
        name='question-update'),
    url(r'^question/(?P<pk>[0-9]+)/delete/$', views.QuestionDelete.as_view(),
        name='question-delete'),

    url(r'^question/(?P<pk>[0-9]+)/add-answer/$', views.AnswerView.as_view(),
        name='answer-create'),
    url(r'^answer/(?P<pk>[0-9]+)/$', views.AnswerUpdate.as_view(),
        name='answer-update'),
    url(r'^answer/(?P<pk>[0-9]+)/delete/$', views.AnswerDelete.as_view(),
        name='answer-delete'),

    # Registration
    url(r'^registration/add/$', views.RegistrationCreate.as_view(),
        name='registration-create'),
    url(r'^registration/thanks/$', views.RegistrationThanksView.as_view(),
        name='registration-thanks'),
    url(r'^registration/$', views.RegistrationIndexView.as_view(),
        name='registration-list'),
    url(r'^registration/(?P<pk>[0-9]+)/$', views.RegistrationUpdate.as_view(),
        name='registration-update'),
    url(r'^registration/(?P<pk>[0-9]+)/delete/$', views.RegistrationDelete.as_view(),
        name='registration-delete'),

    # Announce mail
    url(r'^mail/add/$', views.AnnounceMailCreate.as_view(),
        name='announce-mail-create'),
    url(r'^mail/ok/$', views.AnnounceMailOkView.as_view(),
        name='announce-mail-ok'),
]
