from django.contrib import admin

from .models import News, Question, Answer, Registration, AnnounceMail

admin.site.register(News)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Registration)
admin.site.register(AnnounceMail)
