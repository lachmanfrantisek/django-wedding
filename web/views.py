import re

from django.contrib.auth.models import User
from django.contrib.syndication.views import Feed
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views import generic
from django.views.generic import CreateView, UpdateView, DeleteView, TemplateView

from .models import News, Question, Answer, Registration, AnnounceMail


def main_page(request):
    return render(request, 'web/main_page.html', {'title': "František a Dominika"})


class NewsCreate(CreateView):
    model = News
    fields = ['title', 'text', 'date']
    success_url = reverse_lazy('web:news-list')


class NewsUpdate(UpdateView):
    model = News
    fields = ['title', 'text', 'date', 'send_date']
    success_url = reverse_lazy('web:news-list')


class NewsDelete(DeleteView):
    model = News
    success_url = reverse_lazy('web:news-list')


class NewsIndexView(generic.ListView):
    model = News
    context_object_name = 'all_news'
    ordering = '-date'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "Novinky"
        return ctx


class QuestionCreate(CreateView):
    model = Question
    fields = ['author', 'title', 'text']
    success_url = reverse_lazy('web:question-list')

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        for user in User.objects.all():
            if user.groups.filter(name='notification').exists():
                plaintext = get_template('web/question-email.txt')
                html = get_template('web/question-email.html')
                d = {'question': self.object}
                text_content = plaintext.render(d)
                html_content = html.render(d)

                msg = EmailMultiAlternatives(subject=_("Question"),
                                             from_email="svatba@frantisekadominika.tk",
                                             reply_to=["lachmanfrantisek@gmail.com"],
                                             to=[user.email],
                                             body=text_content)

                msg.attach_alternative(html_content, "text/html")
                msg.send(fail_silently=True)
        return response


class QuestionUpdate(UpdateView):
    model = Question
    fields = ['author', 'title', 'text']
    template_name_suffix = "_private_form"
    success_url = reverse_lazy('web:question-list')


class QuestionDelete(DeleteView):
    model = Question
    template_name_suffix = "_private_form"
    success_url = reverse_lazy('web:question-list')


class QuestionIndexView(generic.ListView):
    model = Question
    context_object_name = 'all_questions'
    ordering = '-date'
    message = None

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['message'] = self.message
        ctx['title'] = "Dotazy"
        return ctx


def question_answer(request):
    try:
        question_id = request.GET["question"]
    except:
        return render(request, 'web/nothing.html', {'title': "Dotaz nenalezen!"})

    question_items = Question.objects.filter(id=question_id)
    if len(question_items) == 1:
        return render(request, 'web/question_answer.html',
                      {'question': question_items[0],
                       'title': question_items[0].title})

    return render(request, 'web/nothing.html', {'title': "Dotaz nenalezen!"})


class AnswerUpdate(UpdateView):
    model = Answer
    fields = ['text', 'date']
    success_url = reverse_lazy('web:question-list')


class AnswerDelete(DeleteView):
    model = Answer
    success_url = reverse_lazy('web:question-list')


class AnswerView(CreateView):
    template_name = "web/question_answer.html"
    model = Answer
    fields = ['text', 'author']
    question = None
    success_url = "/question/"

    def get(self, request, *args, **kwargs):
        question_match = re.match(r'/question/([0-9]+)/add-answer/?', request.path)
        if question_match:

            # Show question
            self.question = Question.objects.get(pk=question_match.group(1))
            self.success_url = "/question/#question{}".format(self.question.id)

            # Set user to authenticated user
            if request.user.is_authenticated():
                default = User.objects.get(pk=request.user.id)
            else:
                default = None
            self.initial = {'author': default}

            return super().get(request, *args, **kwargs)

        return render(request, 'web/nothing.html', {'title': "Dotaz nenalezen!"})

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['question'] = self.question
        return ctx

    def post(self, request, *args, **kwargs):
        try:
            question_match = re.match(r'/question/([0-9]+)/add-answer/?', request.path)
            if question_match:
                question = Question.objects.get(pk=question_match.group(1))
                self.success_url = "/question/#question{}".format(question.id)

                response = super().post(request, *args, **kwargs)

                question.answer = self.object
                question.save()
                return response

            return render(request, 'web/nothing.html', {'title': "Dotaz nenalezen!"})

        except Exception as ex:
            return render(request, 'web/nothing.html', {'title': "Dotaz nenalezen!"})


class NewsFeed(Feed):
    title = "Svatební novinky"
    link = "/news/"
    description = "Novinky k nastávající svatbě dvou nastávajících manželů."

    def items(self):
        return News.objects.order_by('-date')

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.text

    def item_link(self, item):
        return "/news/#news{}".format(item.id)


class RegistrationCreate(CreateView):
    model = Registration
    fields = ['name', 'people_count', 'mail', 'go_to_lunch', 'go_to_party']
    success_url = reverse_lazy('web:registration-thanks')

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        for user in User.objects.all():
            if user.groups.filter(name='notification').exists():
                plaintext = get_template('web/email.txt')
                html = get_template('web/email.html')
                d = {'registration': self.object}
                text_content = plaintext.render(d)
                html_content = html.render(d)

                msg = EmailMultiAlternatives(subject=_("Registration"),
                                             from_email="svatba@frantisekadominika.tk",
                                             reply_to=["lachmanfrantisek@gmail.com"],
                                             to=[user.email],
                                             body=text_content)

                msg.attach_alternative(html_content, "text/html")
                msg.send(fail_silently=True)
        return response


class RegistrationUpdate(UpdateView):
    model = Registration
    fields = ['name', 'people_count', 'mail', 'go_to_lunch', 'go_to_party', 'date']
    success_url = reverse_lazy('web:registration-list')


class RegistrationDelete(DeleteView):
    model = Registration
    success_url = reverse_lazy('web:registration-list')


class RegistrationIndexView(generic.ListView):
    model = Registration
    ordering = '-date'


class RegistrationThanksView(TemplateView):
    def get(self, request, *args, **kwargs):
        return render(request, 'web/thanks.html', {})


class AnnounceMailCreate(CreateView):
    model = AnnounceMail
    fields = ['subject', 'go_to_lunch', 'go_to_party', 'text']
    success_url = reverse_lazy('web:announce-mail-ok')

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        for reg in Registration.objects.all():
            if self.object.go_to_lunch and not reg.go_to_lunch:
                continue
            if self.object.go_to_party and not reg.go_to_party:
                continue

            plaintext = get_template('web/announce-mail.txt')
            html = get_template('web/announce-mail.html')
            d = {
                'content': self.object.text,
                'subject': self.object.subject
            }
            text_content = plaintext.render(d)
            html_content = html.render(d)

            msg = EmailMultiAlternatives(subject=self.object.subject,
                                         from_email="svatba@frantisekadominika.tk",
                                         reply_to=["lachmanfrantisek@gmail.com"],
                                         to=[reg.mail],
                                         body=text_content)

            msg.attach_alternative(html_content, "text/html")
            msg.send(fail_silently=True)
        return response


class AnnounceMailOkView(TemplateView):
    def get(self, request, *args, **kwargs):
        return render(request, 'web/announce-mail-ok.html', {})
