FROM python:3

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client tree vim \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
# COPY . .

ENV VIRTUAL_HOST=frantisekadominika.tk
ENV VIRTUAL_PORT=8000
# ENV LETSENCRYPT_HOST=frantisekadominika.tk
# ENV LETSENCRYPT_EMAIL=lachmanfrantisek@gmail.com
# ENV VIRTUAL_PROTO=uwsgi

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
