��    ]           �      �     �     �     �                    )     2     9     B     J  $   c     �     �     �     �     �     �     �     �     �     �     �     �     	     !	     -	     H	  $   T	     y	     �	     �	     �	     �	     �	     �	  1   �	  1   
  	   >
     H
     O
     W
     d
  	   j
     t
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                <     I     Q     V  	   [  %   e  %   �     �     �     �     �     �  !        .     4     ;  $   M  (   r  '   �  '   �  $   �  #     (   4  +   ]  )   �  %   �  "   �  !   �  &     *   E  *   p     �     �     �     �     �     �     �     �       	        (     @     I     O     X     a  !   {     �  	   �     �  	   �     �     �     �     �     	  
        $     3     E     W     ]     v     �     �     �     �     �     �       
   	       !   3     U     c     r          �     �  $   �     �  
   �     �     �     �          5     B  
   K     V     \     q  
   �  
   �     �     �     �     �     �  	          <        P     f     �     �     �     �  %   �  %   �  +        8     W  (   r      �      �     �  "   �        !   ?  $   a  ,   �  !   �     �     �       
        E   *   F           1   ?             \          N       #      2   >       K   &   (   	       '            ;       O   5   V   <                   !   6   H   U      +   R   [           X         =      C   W       Q       P       )   8   T   Z   D   J      G                      B   .      M               
         ]   4         :      9            I   3       0      -   "       $                   Y          L   %   ,                       7   @   /       S   A        Add Add new Add preamble Add question Answer Article not found! Articles Author Capacity Confirm Confirm gift reservation Confirm the reservation of one piece Contacts Create Create new user Czech Date Dear Weddings Delete Delete answer Delete article Delete gift Delete news Delete preamble Delete registration Description Displayed name of the link Edit answer Email adress, that I am used to read File uploaded at: Gift Gift cannot be reserved! Gift not found Gift reserved! Gifts Gifts/Wishes I am invited to the lunch and I am going to come. I am invited to the party and I am going to come. Link-name Log in Log out Logged user: Lunch Mail send Markdown-formatted text content Name/names New mail New question News No file uploaded. Not answered yet. Number of people Party Preamble Question Question not found! Questions and maybe also answers Registration Reserve Save Send Send date Send only to people going to a lunch. Send only to people going to a party. Subject Text Thanks for registration There is a new question: There is a new registration: There is a new reservation of the Title Upload Upload media file Usage in the text (markdown syntax): You are not allowed to answer questions. You are not allowed to create new user. You are not allowed to delete articles. You are not allowed to delete gifts. You are not allowed to delete news. You are not allowed to delete preambles. You are not allowed to delete registration. You are not allowed to delete the answer. You are not allowed to edit articles. You are not allowed to edit gifts. You are not allowed to edit news. You are not allowed to edit preambles. You are not allowed to upload media files. You are not allowed to view registrations. Your newlyweds Your wedding page. count infinite Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-06-28 10:53+0200
PO-Revision-Date: 2018-06-28 13:14+0200
Last-Translator: 
Language-Team: 
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.8
 Přidat Přidat Přidat předmluvu Přidat dotaz Odpověď Článek nebyl nalezen! Články Autor Kapacita Potvrdit Potvrzení rezervace daru Potvrzení rezervace jednoho kusu Kontakty Vytvořit Vytvořit nového uživatele Čeština Datum Drazí svatebčané Smazat Smazat odpověď Smazat článek Smazat dar Smazat novinku Smazat předmluvu Smazat registraci Popis Zobrazený název odkazu Upravit odpověď Emailová adresa, kterou čtu Soubor nahrán na adresu: Dar Dar nelze rezervovat! Dar nebyl nalezen! Dar byl zarezervován! Dary (Milo)dary Jsem zván na oběd a dorazím Jsem zván na veselici a dorazím Název odkazu Přihlásit se Odhlásit se Přihlášený uživatel: Oběd Mail odeslán Obsah formátovaný pomocí markdown Jméno/Jména Nový mail Nový dotaz Novinky Žádný soubor nebyl nahrán. Zatím nezodpovězeno. Počet lidí Mecheche Předmluva Dotaz Dotaz nebyl nalezen! Otázky a možná i odpovědi Registrace Rezervovat Uložit Odeslat Datum odeslání Odeslat pouze hostům oběda. Odeslat pouze hostům mecheche. Předmět Text Děkujeme za potvrzení účasti. Budeme se na Vás těšit. Přidán nový dotaz: Nové potvrzení účasti: Byl zarezervován dar Nadpis Nahrát Nahrát mediální soubor Využití v textu (markdown syntaxe): Nemáte právo odpovídat na otázky. Nemáte právo vytvářet nové uživatele. Nemáte právo mazat články. Nemáte právo mazat dary. Nemáte právo vytvářet mazat novinky. Nemáte právo mazat předmluvy. Nemáte právo mazat registrace. Nemáte právo mazat odpovědi. Nemáte právo upravovat články. Nemáte právo upravovat dary. Nemáte právo upravovat novinky. Nemáte právo upravovat předmluvy. Nemáte právo nahrávat mediální soubory. Nemáte právo vidět registrace. František a Dominika Vaše svatební stránka počet neomezeně 